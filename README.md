# lean4-study

## Whats is Lean?

Lean is an interactive theorem prover and programming language designed for formalizing mathematics and writing correct and trustworthy software. It is developed by Microsoft Research and the Lean community.

### Required

- [Lean 4](https://leanprover.github.io/download/) or nightly version